#!/bin/bash
SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"
change_var=$(/usr/sbin/aide --check | tee -a "$SCRIPT_DIR"/temp.log | grep "differences between")
current_date=$( date +%Y%m%d)
if [ "$change_var" == "" ]
then
        echo "everythink ok"
else
        echo -e "\e[31m ================================================================================ \e[0m" >> "$SCRIPT_DIR"/report-"$current_date".log
        /usr/sbin/aide --check >> "$SCRIPT_DIR"/report-"$current_date".log
        /usr/sbin/aide --update 2>&1 >> /dev/null
        mv /var/lib/aide/aide.db.gz /var/lib/aide/aide.db."$current_date".gz
        mv /var/lib/aide/aide.db.new.gz /var/lib/aide/aide.db.gz
        echo -e "\e[31m ================================================================================ \e[0m" >> "$SCRIPT_DIR"/report-"$current_date".log
	cat $SCRIPT_DIR/temp.log | grep "added:" | awk '{print $2}' | while read add_i
	do
        	echo "Yaradilan file\'lar: $add_i" >> "$SCRIPT_DIR"/mail.log
	done
	cat $SCRIPT_DIR/temp.log | grep "removed:" | awk '{print $2}' | while read remove_i
	do
        	echo "Silinen file\'lar: $remove_i" >> "$SCRIPT_DIR"/mail.log
	done
	cat $SCRIPT_DIR/temp.log | grep "changed:" | awk '{print $2}' | while read changed_i
	do
        	echo "Deyishilen file\'lar: $changed_i" >> "$SCRIPT_DIR"/mail.log
	done
	cat "$SCRIPT_DIR"/mail.log
	#cat "$SCRIPT_DIR"/mail.log | mailx -v -s "DIQQET $hostname SERVERINDE SYSTEM FILE'DA DEYISHIKLIK VAR" -S smtp=smtp://smtp-adres -S from="no-reply@gmail.com"  nihadbekir@gmail.com
fi
rm -rf "$SCRIPT_DIR"/temp.log
rm -rf "$SCRIPT_DIR"/mail.log

